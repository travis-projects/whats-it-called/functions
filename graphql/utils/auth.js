require('dotenv').config()
const jwt = require('jsonwebtoken')

const { APP_SECRET } = require('../secrets')

function getUserId(token) {
  if (token) {
    const { userId } = jwt.verify(token, APP_SECRET)
    return userId
  }

  throw new AuthError()
}

class AuthError extends Error {
  constructor() {
    super('Not authorized')
  }
}

module.exports = {
  getUserId,
  AuthError
}
