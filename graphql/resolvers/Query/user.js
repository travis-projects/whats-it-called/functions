module.exports = {
  async users (parent, args, ctx, info) {
    await ctx.getUserId(ctx.authorization)
    try {
      const users = await ctx.prisma.query.users(null, info)
      return users
    } catch (error) {
      throw new Error(error.message)
    }
  }
}
