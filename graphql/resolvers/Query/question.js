const question = async (parent, { slug }, ctx, info) => {
  try {
    const checkSlug = async () => {
      const questionExists = await ctx.prisma.exists.Question({ slug })
      if (!questionExists) {
        const { redirect } = await ctx.prisma.query.redirect({ where: { slug } })
        if (!redirect) throw new Error('Question cannot be found')
        return redirect
      }
      return slug
    }
    const question = await ctx.prisma.query.question({ where: { slug: await checkSlug() } }, info)
    return question
  } catch (error) {
    throw new Error(error.message)
  }
}

const questions = async (parent, args, ctx, info) => {
  try {
    const questions = await ctx.prisma.query.questionsConnection({ orderBy: 'createdAt_DESC' }, info)
    return questions
  } catch (error) {
    throw new Error(error.message)
  }
}

const userQuestions = async (parent, { id }, ctx, info) => {
  try {
    const questions = await ctx.prisma.query.questionsConnection({ where: { author: { id } }, orderBy: 'createdAt_DESC' }, info)
    return questions
  } catch (error) {
    throw new Error(error.message)
  }
}

module.exports = {
  question,
  questions,
  userQuestions
}
