require('dotenv').config()
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const { APP_SECRET } = require('../../secrets')

module.exports = {
  async signup(parent, {
    auth: {
      username,
      email,
      password
    },
    user: {
      firstname,
      lastname
    }
  }, ctx, info) {
    try {
      const fullname = `${firstname} ${lastname}`
      const hashedPassword = await bcrypt.hash(password, 12)
      const user = await ctx.prisma.mutation.createUser({
        data: {
          username,
          email,
          password: hashedPassword,
          profile: {
            create: {
              firstname,
              lastname,
              fullname
            }
          }
        },
      })
      const token = await jwt.sign({
        userId: user.id
      }, APP_SECRET)
      return { token, user }
    } catch (e) {
      throw new Error(e.message)
    }
  },
  async login(parent, {
    auth: {
      email,
      password
    }
  }, ctx, info) {
    try {
      const user = await ctx.prisma.query.user({
        where: {
          email
        }
      })
      if (!user) throw new Error(`No such user found for email: ${email}`)
      const valid = await bcrypt.compare(password, user.password)
      if (!valid) throw new Error('Invalid password')
      return {
        token: jwt.sign({
          userId: user.id
        }, APP_SECRET),
        user,
      }
    } catch (e) {
      throw new Error(e.message)
    }
  },
  // async verify(parent, { verificationCode }, ctx, info) {
  //   try {
  //     // Check code is good, if so verify
  //     const user = await ctx.db.query.user({ where: { verificationCode } })
  //     if (!user) throw new Error(`Verification code is invalid!`)
  //     await ctx.db.mutation.updateUser({
  //       where: { id: user.id },
  //       data: { emailVerified: true }
  //     })
  //     await newUserEmail(user)
  //     return true
  //   } catch (error) {
  //     throw new Error(error.message)
  //   }
  // },
  // async resendVerificationCode(parent, args, ctx, info) {
  //   // userID
  //   const userId = ctx.getUserId(ctx)
  //   try {
  //     // Reset & Resend verification code
  //     const verificationCode = randomString()
  //     const user = await ctx.db.mutation.updateUser({
  //       where: { id: userId },
  //       data: { verificationCode }
  //     }).then(user => user).catch(err => err)
  //     await verificationCodeEmail(user, verificationCode)
  //     return true
  //   } catch (error) {
  //     throw new Error(error.message)
  //   }
  // }
}
