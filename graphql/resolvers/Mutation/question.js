const nanoid = require('nanoid')
const slugify = require('slugify')

const createQuestion = async (parent, { question }, ctx, info) => {
  const userId = await ctx.getUserId(ctx.authorization)
  const { title, content } = question
  try {
    const slug = `${slugify(title)}-${nanoid(8)}`
    const question = await ctx.prisma.mutation.createQuestion({
      data: {
        slug,
        title,
        content,
        author: {
          connect: {
            id: userId
          }
        }
      }
    }, info)
    return question
  } catch (error) {
    throw new Error(error.message)
  }
}

const updateQuestionTitle = async (parent, { question }, ctx, info) => {
  const userId = await ctx.getUserId(ctx.authorization)
  const { slug, title } = question
  const hasPermission = await ctx.prisma.exists.Question({ slug, author: { id: userId } })
  if (!hasPermission) throw new Error('Not Authorized')
  const newSlug = async () => {
    const newSlug = `${slugify(title)}-${nanoid(8)}`
    await ctx.prisma.mutation.createRedirect({ data: { slug, redirect: newSlug }})
    return newSlug
  }
  try {
    const question = await ctx.prisma.mutation.updateQuestion({
      where: { slug },
      data: {
        slug: await newSlug(),
        title
      }
    }, info)
    return question
  } catch (error) {
    throw new Error(error.message)
  }
}

const updateQuestionContent = async  (parent, { question }, ctx, info) => {
  const userId = await ctx.getUserId(ctx.authorization)
  const { slug, content } = question
  const hasPermission = await ctx.prisma.exists.Question({ slug, author: { id: userId } })
  if (!hasPermission) throw new Error('Not Authorized')
  try {
    const question = await ctx.prisma.mutation.updateQuestion({
      where: { slug },
      data: {
        slug,
        content
      }
    }, info)
    return question
  } catch (error) {
    throw new Error(error.message)
  }
}

const deleteQuestion = async (parent, { slug }, ctx, info) => {
  const userId = await ctx.getUserId(ctx.authorization)
  const hasPermission = await ctx.prisma.exists.Question({ slug, author: { id: userId } })
  if (!hasPermission) throw new Error('Not Authorized')
  try {
    const question = await ctx.prisma.mutation.deleteQuestion({ where: { slug } }, info)
    return question
  } catch (error) {
    throw new Error(error.message)
  }
}


module.exports = {
  createQuestion,
  updateQuestionTitle,
  updateQuestionContent,
  deleteQuestion
}
