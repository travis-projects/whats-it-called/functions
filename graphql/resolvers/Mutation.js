const auth = require('./Mutation/auth')
const question = require('./Mutation/question')

module.exports = {
  ...auth,
  ...question
}
