const user = require('./Query/user')
const question = require('./Query/question')

module.exports = {
  ...user,
  ...question
}
