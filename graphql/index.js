require('dotenv').config()
const { ApolloServer } = require('apollo-server-azure-functions')
const { Prisma } = require('prisma-binding')
const { getUserId } = require('./utils/auth')

const { importSchema } = require('graphql-import')

const resolvers = require('./resolvers')
const typeDefs = importSchema('graphql/schema/schema.graphql')

const { PRISMA_ENDPOINT, PRISMA_SECRET } = require('./secrets.js')
const prisma = new Prisma({
  typeDefs: 'graphql/schema/generated/prisma.graphql',
  endpoint: PRISMA_ENDPOINT,
  secret: PRISMA_SECRET
})


const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ request, context }) => ({
    authorization: request.headers.authorization,
    prisma,
    getUserId,
    request,
    context
  }),
  introspection: true,
  playground: true
})

module.exports = server.createHandler({
  cors: {
    origin: true,
    credentials: true,
  },
});
